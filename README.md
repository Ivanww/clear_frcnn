# A Clear Implementation of faster-RCNN

Another simple but clear implementation of faster-RCNN.

**Target**

1. Separate the logics of data (images, anchors) processing from the network architecture itself.
2. Wrap the helper functions (anchor handling, image resizing etc.) into module. This helps with using higher level training framework (like Catalyst) to train the model
3. In current stage, Implementation is mainly based on the `torchvision`.
   1. Use VGG16 as backbone
   2. No FPN for the backbone
   3. Use VOC2007 "trainval" split for training, use VOC2007 test split for testing.
4. Features coming in the feature:
   1. Backbone with FPN

**Limitations**

1. `batch_size` should be 1. The preprocessing module only resize the input images. Images of different sizes can not be stacked as one tensor after preprocessing.
   1. Solution: Use "letter box" method to wrap the resized images.
2. Arguments type, shape checks are limited. It's a little hard to debug.
3. Efficiency of detection need to be improved

**Attentions**

1. Use "YXYX" format for box coordinates. (Usually it is "XYXY"). Pay attention to the format of anchors, ground truth boxes etc.
2. VOC dataset's coordinates are 1-based. I don't handle this issue in the implementation.
3. Evaluation are performed on the preprocessed images and boxes, not on the original image. To actually draw the final box on original image, you need to resize the predicted box according to  the original images

**Milestones**

- [x] Models
  - [x] Backbone, use VGG16 for now
  - [x] Regional Proposal Network
  - [x] ROI Head
- [x] Metrics
  - [x] Average Precision
  - [x] Mean Average Precision Meter
- [x] Data Processing
  - [x] AnchorHandler, generate anchors
  - [x] RCNN preprocessing, resizing images and boxes; moving to device
  - [x] RCNN postprocessing, decoding images, removing boxes
- [x] Loss function
  - [x] Assign target to labels
  - [x] RPN loss function
  - [x] ROI head loss function

**Validation**

TBD

**Reference**

https://github.com/chenyuntc/simple-faster-rcnn-pytorch

