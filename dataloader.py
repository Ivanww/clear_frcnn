import os
import xml.etree.ElementTree as ET
import numpy as np
from PIL import Image

def read_image(path, dtype=np.float32, color=True):
    f = Image.open(path)
    img = f.convert('RGB')
    img = np.asarray(img, dtype=dtype)
    f.close()

    return img.transpose((2, 0, 1))

VOC_BBOX_LABEL_NAMES = (
    'aeroplane', 'bicycle','bird','boat','bottle','bus',
    'car','cat','chair','cow','diningtable','dog','horse','motorbike',
    'person','pottedplant','sheep','sofa','train','tvmonitor')

from torch.utils.data import Dataset
class VOCBboxDataset(Dataset):
    def __init__(self, data_dir, split='trainval',
                 use_difficult=False):
        super(VOCBboxDataset, self).__init__()
        id_list_file = os.path.join(
            data_dir, 'ImageSets/Main/{0}.txt'.format(split))

        self.ids = [id_.strip() for id_ in open(id_list_file)]
        self.data_dir = data_dir
        self.use_difficult = use_difficult
        self.label_names = VOC_BBOX_LABEL_NAMES
        print('' if self.use_difficult else 'Not', ' Use Difficult Labels')

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, i):
        id_ = self.ids[i]
        anno = ET.parse(
            os.path.join(self.data_dir, 'Annotations', id_ + '.xml'))
        bbox = list()
        label = list()
        difficult = list()
        for obj in anno.findall('object'):
            if not self.use_difficult and int(obj.find('difficult').text) == 1:
                continue

            difficult.append(int(obj.find('difficult').text))
            bndbox_anno = obj.find('bndbox')
            # subtract 1 to make pixel indexes 0-based
            bbox.append([
                int(bndbox_anno.find(tag).text) - 1
                for tag in ('ymin', 'xmin', 'ymax', 'xmax')])
            name = obj.find('name').text.lower().strip()
            label.append(VOC_BBOX_LABEL_NAMES.index(name))

        bbox = np.stack(bbox).astype(np.float32)
        label = np.stack(label).astype(np.int32)
        difficult = np.array(difficult, dtype=np.bool).astype(np.uint8)

        # Load a image
        img_file = os.path.join(self.data_dir, 'JPEGImages', id_ + '.jpg')
        img = read_image(img_file, color=True)

        return img, bbox, label, difficult

class VOCTestDataset(VOCBboxDataset):
    def __init__(self, datadir):
        super(VOCTestDataset, self).__init__(datadir, 'test', use_difficult=True)

    def __getitem__(self, i):
        images, boxes, labels, difficult = super(VOCTestDataset, self).__getitem__(i)
        size = images.shape[1:]

        return {'images': images, 'boxes': boxes, 'sizes': size,
            'labels': labels, 'difficult': difficult}

class VOCTrainDataset(VOCBboxDataset):
    def __init__(self, datadir):
        super(VOCTrainDataset, self).__init__(datadir, 'trainval', use_difficult=True)

    def __getitem__(self, i):
        images, boxes, labels, difficult = super(VOCTrainDataset, self).__getitem__(i)
        size = images.shape[1:] # original size

        return {'images': images, 'boxes': boxes, 'sizes': size,
            'labels': labels, 'difficult': difficult}

def collate_fn(batch):
    collated = {}
    collated['images'] = [b['images'] for b in batch]
    collated['boxes'] = [b['boxes'] for b in batch]
    collated['labels'] = [b['labels'] for b in batch]
    collated['difficult'] = [b['difficult'] for b in batch]
    collated['sizes'] = [b['sizes'] for b in batch]

    return collated

from torch.utils.data import DataLoader
def load_train_data():
    dataset = VOCTrainDataset('/home/ivan/data/VOC_train/VOCdevkit/VOC2007')

    train_loader = DataLoader(dataset, batch_size=1, shuffle=True, collate_fn=collate_fn,
            num_workers=2)

    return train_loader
def load_test_data():
    dataset = VOCTestDataset('/home/ivan/data/VOC_train/VOCdevkit/VOC2007')

    test_loader = DataLoader(dataset, batch_size=1, shuffle=False, collate_fn=collate_fn,
            num_workers=2)

    return test_loader
