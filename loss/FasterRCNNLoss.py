import torch
import torch.nn as nn

from .RPNLoss import RPNLoss
from .ROILoss import ROILoss

class FasterRCNNLoss(nn.Module):
    def __init__(self, rpn_loss_cfg={}, head_loss_cfg={}):
        super(FasterRCNNLoss, self).__init__()
        self.rpn_loss = RPNLoss(**rpn_loss_cfg)
        self.head_loss = ROILoss(**head_loss_cfg)

    def forward(self, x, targets):
        roi_loss = self.head_loss(x, targets)
        roi_total_loss = roi_loss[0] + roi_loss[1]

        rpn_loss = self.rpn_loss(x, targets)
        rpn_total_loss = rpn_loss[0] + rpn_loss[1]

        return  {'rpn_loss': rpn_total_loss, 'roi_loss': roi_total_loss,
                'total_loss': rpn_total_loss + roi_total_loss,
                'rpn_box': rpn_loss[0], 'rpn_score': rpn_loss[1],
                'roi_box': roi_loss[1], 'roi_score': roi_loss[0]}
