import torch
import torch.nn as nn

from .utils import TargetSampler
from .BBoxRegressionLoss import BBoxRegressionLoss

class ClassProbLoss(nn.Module):
    def __init__(self, ignore=-1, background=0):
        super(ClassProbLoss, self).__init__()
        self.IGNORE = ignore
        self.BACKGROUND = background
        self.loss_fn = nn.CrossEntropyLoss(ignore_index=self.IGNORE)

    def forward(self, x, labels):
        labels = labels.view(-1)
        return self.loss_fn(x, labels)

class ROILoss(nn.Module):
    def __init__(self, coord_weights=[10, 10, 5, 5], conf_lb=0.5, conf_ub=0.5, num_prop=512, pos_ratio=0.25):
        super(ROILoss, self).__init__()

        self.sampler = TargetSampler(conf_lb, conf_ub, num_prop, pos_ratio)

        self.class_loss = ClassProbLoss(ignore=self.sampler.IGNORED,
            background=self.sampler.BACKGROUND)
        self.box_reg_loss = BBoxRegressionLoss(coord_weights=coord_weights,
            ignore=self.sampler.IGNORED, background=self.sampler.BACKGROUND,
            smooth=True)

    def forward(self, x, targets=None):
        box_scores = x['box_scores'] # predictions
        bbox_reg = x['boxes']

        proposals = x['proposals'] # targets
        batch_index = x['batch_index']
        batch_labels = x['proposal_labels']
        proposal_boxes = x['proposal_boxes']

        batch_labels = torch.cat(batch_labels, dim=0)
        batch_proposals = [proposals[batch_index==i] for i in range(batch_index[-1]+1)]
        cls_loss = self.class_loss(box_scores, batch_labels)
        box_loss = self.box_reg_loss(bbox_reg.view(bbox_reg.size(0), -1, 4),
                proposal_boxes, batch_proposals, batch_labels)

        return cls_loss,  box_loss
