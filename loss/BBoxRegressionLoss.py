import torch
import torch.nn as nn
import torch.nn.functional as F

class BBoxRegressionLoss(nn.Module):
    def __init__(self, coord_weights = [10, 10, 5, 5], ignore=-1,
            background=0, smooth=True, smooth_beta=1.):
        super(BBoxRegressionLoss, self).__init__()
        self.IGNORE = ignore
        self.BACKGROUND = background
        coord_weights = torch.tensor(coord_weights)
        self.register_buffer('coord_weights', coord_weights)
        self.smooth_beta = smooth_beta
        if smooth:
            self.loss_fn = nn.SmoothL1Loss(reduction='sum', beta=self.smooth_beta)
        else:
            self.loss_fn = nn.L1Loss(reduction='sum')


    def encode_box(self, boxes, anchors):
        box_h = boxes[:, 2] - boxes[:, 0]
        box_w = boxes[:, 3] - boxes[:, 1]
        box_cy = boxes[:, 0] + 0.5 * box_h
        box_cx = boxes[:, 1] + 0.5 * box_w

        anchor_h = anchors[:, 2] - anchors[:, 0]
        anchor_w = anchors[:, 3] - anchors[:, 1]
        anchor_cy = anchors[:, 0] + 0.5 * anchor_h
        anchor_cx = anchors[:, 1] + 0.5 * anchor_w

        dx = self.coord_weights[0] * (box_cx - anchor_cx) / anchor_w
        dy = self.coord_weights[1] * (box_cy - anchor_cy) / anchor_h
        dw = self.coord_weights[2] * torch.log(box_w / anchor_w)
        dh = self.coord_weights[3] * torch.log(box_h / anchor_h)

        encoded = torch.stack((dy, dx, dh, dw), dim=1)
        return encoded

    def forward(self, deltas, boxes, ref, labels):
        # encode boxes with ref
        if isinstance(ref, list):
            # when different ref is provided
            assert len(boxes) == len(ref)
            t_delta = torch.cat([self.encode_box(b, a) for b, a in zip(boxes, ref)], dim=0)
        else:
            # use the same ref box for all deltas
            t_delta = torch.cat([self.encode_box(b, ref) for b in boxes], dim=0)

        t_mask = torch.nonzero((labels != self.IGNORE) & (labels != self.BACKGROUND),
            as_tuple=True)[0]

        if len(deltas.shape) > 2:
            pred_boxes = deltas.view(labels.size(0), -1, 4)
            # need extra class label to select correct pred box
            pred_boxes = pred_boxes[t_mask, labels[t_mask], :]
        else:
            pred_boxes = deltas[t_mask]

        t_delta = t_delta.view(-1, 4)[t_mask]
        numel = (labels != self.IGNORE).sum()
        if numel == 0:
            print(f'0 Boxes, pred_boxes {pred_boxes.shape}, target {t_delta.shape}')
        reg_loss = self.loss_fn(pred_boxes, t_delta)

        return reg_loss/numel
