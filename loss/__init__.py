from .BBoxRegressionLoss import BBoxRegressionLoss
from .ObjectConfidenceLoss import ObjectConfidenceLoss
from .ROILoss import ROILoss
from .RPNLoss import RPNLoss

from .FasterRCNNLoss import FasterRCNNLoss
