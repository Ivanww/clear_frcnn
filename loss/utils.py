import torch
import torch.nn as nn

def bbox_iou(box1, box2):
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[..., 0], box1[..., 1], box1[..., 2], box1[..., 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[..., 0], box2[..., 1], box2[..., 2], box2[..., 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)


    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1, min=0
    )

    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area)

    return iou

class TargetSampler(nn.Module):
    BACKGROUND = 0
    LOWQUALITY = -2
    IGNORED = -3
    def __init__(self, conf_lb, conf_ub, batch_size, pos_ratio, remove_outside_box=False):
        super(TargetSampler, self).__init__()

        self.conf_lb = conf_lb
        self.conf_ub = conf_ub
        self.batch_size = batch_size
        self.pos_rato = pos_ratio

        self.remove_outside_box = remove_outside_box

    def _filter_sample_boxes(self, sample_boxes, H, W):
        index_inside = (sample_boxes[:, 0] >= 0) & (sample_boxes[:, 1] >= 0) & \
            (sample_boxes[:, 2] <= H) & (sample_boxes[:, 3] <= W)

        return index_inside

    def _fix_low_quality(self, ious, t_labels, labels):
        max_anchor_iou, match_anchor_idx = ious.max(dim=0)
        # assign object labels to the anchor with maximum iou
        for i, iou in enumerate(max_anchor_iou.view(-1)):
            max_anchor_iou_mask = ious[:, i] == iou
            t_labels[max_anchor_iou_mask] = labels[i]

        # ignore the rest anchors
        t_labels[t_labels==TargetSampler.LOWQUALITY] = TargetSampler.IGNORED

        return t_labels

    def forward(self, true_boxes, sample_boxes, size=None, true_labels=None):
        device = sample_boxes.device

        if true_labels is None:
            true_labels = torch.ones((true_boxes.size(0),), device=device).long().view(-1)
        else:
            true_labels += 1 # class label starts from 1, and 0 is for background

        ious = bbox_iou(sample_boxes.unsqueeze(1), true_boxes.unsqueeze(0))
        #, [A, O], assign an object to each anchor
        max_obj_iou, matched_obj_idx = ious.max(dim=1)

        # initialize anchors's label with object's label
        t_labels = true_labels[matched_obj_idx] # target labels
        t_boxes = true_boxes[matched_obj_idx, :] # target boxes

        t_labels[max_obj_iou <= self.conf_lb] = TargetSampler.BACKGROUND
        if self.conf_lb != self.conf_ub:
            t_labels[(max_obj_iou > self.conf_lb) & (max_obj_iou < self.conf_ub)] = TargetSampler.LOWQUALITY
            self._fix_low_quality(ious, t_labels, true_labels)

        # remove outside anchors
        if self.remove_outside_box:
            assert size is not None
            index_inside = self._filter_sample_boxes(sample_boxes, size[0], size[1])
            t_labels[~index_inside] = TargetSampler.IGNORED
            t_boxes[~index_inside] = 0.

        pos = torch.nonzero(t_labels > TargetSampler.BACKGROUND, as_tuple=True)[0]
        neg = torch.nonzero(t_labels == TargetSampler.BACKGROUND, as_tuple=True)[0]

        num_pos = min(int(self.batch_size*self.pos_rato), pos.numel())
        num_neg = min(neg.numel(), self.batch_size - num_pos)

        pos_idx = torch.randperm(pos.numel(), device=device)[:num_pos]
        neg_idx = torch.randperm(neg.numel(), device=device)[:num_neg]

        sample_labels = torch.full_like(t_labels, TargetSampler.IGNORED).long()
        sample_labels[pos[pos_idx]] = t_labels[pos[pos_idx]].long()
        sample_labels[neg[neg_idx]] = TargetSampler.BACKGROUND
        return t_boxes, sample_labels

