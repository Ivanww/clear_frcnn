import torch
import torch.nn as nn

from .utils import TargetSampler
from .BBoxRegressionLoss import BBoxRegressionLoss

class ObjectConfidenceLoss(nn.Module):
    def __init__(self, ignored=0):
        super(ObjectConfidenceLoss, self).__init__()
        self.IGNORE = ignored

    def forward(self, x, indicator):
        conf_mask = indicator != self.IGNORE

        t_conf = indicator[conf_mask].view(-1)
        pred_conf = x[conf_mask, :]
        if x.size(-1) > 1:
            return F.cross_entropy(pred_conf, t_conf, ignore_index=self.IGNORE)
        else:
            return F.binary_cross_entropy_with_logits(pred_conf, t_conf

class RPNLoss(nn.Module):
    def __init__(self, conf_lb=0.3, conf_ub=0.7, num_prop=256,
            pos_ratio=0.5, box_smooth=1./9):
        super(RPNLoss, self).__init__()

        self.sampler = TargetSampler(conf_lb, conf_ub, num_prop,
            pos_ratio, remove_outside_box=True)
        self.box_reg_loss = BBoxRegressionLoss(coord_weights=[1.,1.,1.,1.], smooth=True, ignore=self.sampler.IGNORED,
            background=self.sampler.BACKGROUND, smooth_beta=box_smooth)
        self.conf_loss = ObjectConfidenceLoss(self.sampler.IGNORED)

    def forward(self, x, batch):
        cls_tensor, reg_tensor = x['rpn_scores'], x['rpn_reg']
        anchors = batch['anchors']
        feat_size = batch['feat_size']

        reg_tensor = reg_tensor.view(-1, 4)
        # for the whole batch
        obj_boxes = [self.sampler(b, anchors, feat_size) for b in batch['boxes']]
        batch_labels = torch.cat([b[1] for b in obj_boxes], dim=0)

        # score tensor use cross entropy loss
        conf_loss = self.conf_loss(cls_tensor.view(-1, 2), batch_labels)

        # box will be encoded and merged in box regression function
        batch_boxes = [b[0] for b in obj_boxes]
        reg_loss = self.box_reg_loss(reg_tensor.view(-1, 4),
            batch_boxes, anchors, batch_labels)

        return reg_loss,  conf_loss
