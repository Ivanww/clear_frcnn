import copy
import numpy as np

class AveragePrecisionMetric(object):
    def __init__(self, use_VOC07=True):
        super(AveragePrecisionMetric, self).__init__()
        self.use_VOC07 = use_VOC07

    def _sort_rec_prec(self, stats, obj_count):
        if obj_count == 0:
            if stats.shape[0] == 0:
                return None, None
            else:
                return np.zeros(stats.shape[0]), np.zeros(stats.shape[0])
        if stats.shape[0] == 0:
            return np.zeros(stats.shape[0]), np.zeros(stats.shape[0])

        sorted_ind = stats[:, 0].argsort()[::-1]
        stats = stats[sorted_ind, :]

        acc_tp = np.cumsum(stats[:, 2]==1, axis=0)
        acc_fp = np.cumsum(stats[:, 2]==0, axis=0)

        prec = np.zeros_like(acc_tp, dtype=np.float)
        pred_count = acc_tp + acc_fp
        non_zeros = pred_count != 0
        prec[non_zeros] = acc_tp[non_zeros] / pred_count[non_zeros]
        rec = acc_tp / obj_count

        sorted_rec_ind = np.argsort(rec, kind='stable')
        sorted_rec = rec[sorted_rec_ind]
        sorted_prec = prec[sorted_rec_ind]

        return sorted_rec, sorted_prec

    def _evaluate_voc07(self, recall, precision):
        ap = 0.

        for rec_thres in np.arange(0, 1.1, 0.1):
            if np.sum(recall >= rec_thres) != 0:
                ap += np.max(precision[recall >= rec_thres]) / 11

        return ap

    def _evaluate_ap_normal(self, recall, precision):
        ap = 0.

        cur_rec = 0.
        while cur_rec < recall.max():
            max_prec_ind = np.argsort(precision[recall> cur_rec], kind='stable')[-1]
            max_prec = precision[recall > cur_rec][max_prec_ind]
            if max_prec == 0:
                break
            next_rec = recall[recall> cur_rec][max_prec_ind]
            accu = (max_prec * (next_rec - cur_rec))
            ap += accu
            cur_rec = next_rec

        return ap

    def evaluate(self, stats, num_object):
        recall, precision = self._sort_rec_prec(stats, num_object)
        if recall is None or precision is None:
            return -1.

        if self.use_VOC07:
            return self._evaluate_voc07(recall, precision)
        else:
            return self._evaluate_ap_normal(recall, precision)

