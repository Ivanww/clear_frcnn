import torch
import sys, os
import numpy as np
from tqdm import tqdm
# sys.path.append('/Users/ivanw/workspace/frcnn')
from collections import OrderedDict

from models import FasterRCNN_VGG16

from utils import AnchorHandler
from utils import RCNNPreProcess
from utils import RCNNPostProcess

from MAPMeter import AveragePrecisionMeter

from dataloader import load_test_data

tmp_root = '/Users/ivanw/workspace/catalyst_rcnn/test/tmp/'

def init_model():
    arch = FasterRCNN_VGG16()

    model_weights = '/home/ivan/workspace/frcnn/model.pth'
    model = torch.load(model_weights)['model']

    ckpt = OrderedDict()

    for k in model:
        if k.startswith('extractor'):
            ckpt[k.replace('extractor', 'backbone')] = model[k]
        elif k.startswith('rpn'):
            ckpt[k.replace('rpn', 'rpn.head')] = model[k]
        else:
            ckpt[k] = model[k]

    arch.load_state_dict(ckpt)
    return arch

def eval_fasterRCNN():
    eval_loader = load_test_data()
    model = init_model()
    model = model.cuda()
    anchor_handler = AnchorHandler()

    pre_process = RCNNPreProcess()

    evaluation_config = {
        'conf_thres':0.05,
        'nms_thres': 0.3,
        'coord_weights': [10, 10, 5, 5],
        'num_class': 20
    }
    post_process = RCNNPostProcess(**evaluation_config).cuda()

    mAP_meter = AveragePrecisionMeter(20)
    mAP_meter.initialize()

    STRIDE = 16
    model.eval()

    for i, batch in enumerate(tqdm(eval_loader)):
        # print('Assume Feature Map Shape: ', (batch['sizes'][0][0] // STRIDE, batch['sizes'][0][1] // STRIDE))
        batch = pre_process('cuda:0', batch)
        feature_size = batch['images'].shape[-2:]
        batch['anchors'] = anchor_handler('cuda:0', STRIDE,
            (feature_size[0] // STRIDE, feature_size[1] // STRIDE))
        outs = model(batch)

        box_reg = outs['boxes']
        box_scores = outs['box_scores']
        proposals = outs['proposals']
        batch_index = outs['batch_index']

        batch_detection = []
        for b in range(batch_index[-1]+1):
            mask = batch_index == b
            scale = batch['scales'][b]
            fea_size = batch['images'].shape[-2:]
            detection = post_process(box_reg[mask],
                box_scores[mask], proposals[mask], fea_size, scale)
            detection['labels'] -= 1

            target = {'boxes': batch['boxes'][b].cpu().numpy(),
                'labels': batch['labels'][b].cpu().numpy(),
                'difficult': batch['difficult'][b], 'scale': scale}

            mAP_meter.accumulate(detection, target)
            batch_detection.append(detection)

        # save sample images

        if i == 50:
            print(detection)
            print(target)
            break

    mAP = mAP_meter.evaluate()
    print(mAP)
    print(f'VOC mean AP {mAP[mAP >= 0].mean():.4f}')

if __name__ == "__main__":
    eval_fasterRCNN()
