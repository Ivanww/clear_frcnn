import torch
import copy
import numpy as np

from metrics import AveragePrecisionMetric
from MetricMeter import MetricMeter
def bbox_iou(box1, box2):
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[..., 0], box1[..., 1], box1[..., 2], box1[..., 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[..., 0], box2[..., 1], box2[..., 2], box2[..., 3]

    inter_rect_x1 = np.maximum(b1_x1, b2_x1)
    inter_rect_y1 = np.maximum(b1_y1, b2_y1)
    inter_rect_x2 = np.minimum(b1_x2, b2_x2)
    inter_rect_y2 = np.minimum(b1_y2, b2_y2)


    inter_area = np.clip(inter_rect_x2 - inter_rect_x1, 0, None) * np.clip(
        inter_rect_y2 - inter_rect_y1, 0, None)

    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area)

    return iou


# ref: https://github.com/rafaelpadilla/Object-Detection-Metrics
class AveragePrecisionMeter(MetricMeter):
    def __init__(self, num_classes, iou_thres=0.5, use_VOC07=True):
        super(AveragePrecisionMeter, self).__init__()
        self.num_classes = num_classes
        self.iou_thres = iou_thres

        self.metric = AveragePrecisionMetric(use_VOC07)

    def initialize(self):
        self.cached_data = {'stats': [], 'obj_count':np.zeros((self.num_classes))}

    def _evaluate_boxes(self, boxes, labels, scores, t_boxes, t_labels, difficult):
        evaluation = np.zeros_like(labels, dtype=np.float32)

        # l ~ [0, 19]
        for l in np.unique(np.concatenate([labels, t_labels])).astype(np.int):
            cls_mask =  labels == l
            cls_boxes = boxes[cls_mask]
            cls_scores = scores[cls_mask]

            order = cls_scores.argsort()[::-1]
            cls_boxes = cls_boxes[order]
            cls_scores = cls_scores[order]

            t_cls_mask = t_labels == l
            t_cls_boxes = t_boxes[t_cls_mask]
            t_cls_difficult = difficult[t_cls_mask]

            self.cached_data['obj_count'][l] += np.logical_not(t_cls_difficult).sum()

            if len(cls_boxes) == 0: # no detections
                continue
            if len(t_cls_boxes) == 0: # no ground truth
                continue # evaluation is 0 by default

            iou = bbox_iou(cls_boxes[:, None, :], t_cls_boxes[None, :, :])
            proposal_labels = iou.argmax(axis=1) # potential evaluation
            proposal_labels [iou.max(axis=1) < self.iou_thres] = -1

            matched = np.zeros(t_cls_boxes.shape[0])
            for i in range(len(proposal_labels)):
                if proposal_labels[i] >= 0:
                    if t_cls_difficult[proposal_labels[i]]:
                        proposal_labels[i] = -1
                    elif matched[proposal_labels[i]] == 0:
                        matched[proposal_labels[i]] = 1
                        proposal_labels[i] = 1 # TP detection
                    else:
                        proposal_labels[i] = 0 # FP detection
                else:
                    proposal_labels[i] = 0
            evaluation[cls_mask] = proposal_labels

        return evaluation

    def accumulate(self, detections, targets):
        # labels, scores, boxes could be [0, :]
        boxes = detections['boxes']
        labels = detections['labels']
        scores = detections['scores']

        # NOTE: targets are not [0, :] for VOC
        t_boxes = targets['boxes']
        t_labels = targets['labels']
        t_difficult = targets['difficult']

        # compute stat for each image
        evaluation = self._evaluate_boxes(boxes, labels, scores, t_boxes, t_labels, t_difficult)
        stats = np.stack([scores, labels.astype(float), evaluation], axis=-1)
        self.cached_data['stats'].append(stats)

    def _evaluate_ap(self):
        stats = np.concatenate(self.cached_data['stats'], axis=0)

        # convert to numpy array for stable sort
        ap_cls = np.ones(self.num_classes) * -1
        # calculate precision & recall for each class
        for l in range(0, self.num_classes):
            num_objects = self.cached_data['obj_count'][l]
            if num_objects == 0:
                continue

            cls_stats = stats[stats[:, 1].astype(np.int) == l].copy()
            ap_cls[l] = self.metric.evaluate(cls_stats, num_objects)

        return ap_cls

    def evaluate(self):
        if len(self.cached_data['stats']) == 0:
            return 0.
        else:
            ap_cls = self._evaluate_ap()
            return ap_cls

    def clear(self):
        self.cached_data = {'stats': [], 'obj_count':np.zeros(self.num_classes)}
