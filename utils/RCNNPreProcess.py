import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import Normalize

class RCNNPreProcess(nn.Module):
    def __init__(self, min_size=600, max_size=1000):
        super(RCNNPreProcess, self).__init__()
        self.min_size = min_size
        self.max_size = max_size

        self.normalize = Normalize(mean=[0.485, 0.456, 0.406],
                                std=[0.229, 0.224, 0.225])

    def _resize(self, device, img, box=None, labels=None):
        img = img / 255.

        # Original Size
        img_size = torch.tensor(img.shape[-2:]).long()
        img = torch.from_numpy(img)

        scale = self.min_size / float(torch.min(img_size))
        if float(torch.max(img_size)) * scale > self.max_size:
            scale = self.max_size / float(torch.max(img_size))
        img = F.interpolate(img[None], scale_factor=scale, mode='bilinear', align_corners=False)[0]
        img = self.normalize(img)
        img = img.to(device)

        if box is not None:
            box = torch.from_numpy(box)
            resized_box = torch.zeros_like(box).float()
            resized_box[:, 0::2] = box[:, 0::2] * scale
            resized_box[:, 1::2] = box[:, 1::2] * scale
            resized_box = resized_box.to(device)
        else:
            resized_box = None

        if labels is not None:
            labels = torch.from_numpy(labels)
            labels = labels.to(device)


        return img, resized_box, labels, scale

    def forward(self, device, batch):# image, boxes=None, labels=None):
        tensor_images = []
        boxes = batch.get('boxes', None)
        if boxes is None:
            boxes = [None] * len(batch['images'])
        tensor_boxes = []
        labels = batch.get('labels', None)
        if labels is None:
            labels = [None] * len(batch['images'])
        tensor_labels = []
        scales = []

        for i, b, l in zip(batch['images'], boxes, labels):
            img, box, label, scale = self._resize(device, i, b, l)
            tensor_images.append(img)
            tensor_boxes.append(box)
            tensor_labels.append(label)
            scales.append(scale)

        batch.update({'images': torch.stack(tensor_images, dim=0),
            'boxes': tensor_boxes, 'labels': tensor_labels, 'scales': scales})

        return batch

