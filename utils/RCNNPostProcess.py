import math
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F

def decoding_box(reg, anchors, max_wh = math.log(1000 / 16.)):
    # use YXYX format, convert to CYCX
    heights = anchors[:, 2] - anchors[:, 0]
    widths = anchors[:, 3] - anchors[:, 1]
    ctr_y = anchors[:, 0] + 0.5 * heights
    ctr_x = anchors[:, 1] + 0.5 * widths

    dy = reg[:, 0::4]
    dx = reg[:, 1::4]
    dh = reg[:, 2::4]
    dw = reg[:, 3::4]

    # Prevent sending too large values into torch.exp()
    dw = torch.clamp(dw, max=max_wh)
    dh = torch.clamp(dh, max=max_wh)

    # Apply anchors
    pred_ctr_x = dx * widths[:, None] + ctr_x[:, None]
    pred_ctr_y = dy * heights[:, None] + ctr_y[:, None]
    pred_w = torch.exp(dw) * widths[:, None]
    pred_h = torch.exp(dh) * heights[:, None]
    # CYCX --> YXYX
    pred_boxes1 = pred_ctr_x - torch.tensor(0.5, dtype=pred_ctr_x.dtype, device=pred_w.device) * pred_w
    pred_boxes2 = pred_ctr_y - torch.tensor(0.5, dtype=pred_ctr_y.dtype, device=pred_h.device) * pred_h
    pred_boxes3 = pred_ctr_x + torch.tensor(0.5, dtype=pred_ctr_x.dtype, device=pred_w.device) * pred_w
    pred_boxes4 = pred_ctr_y + torch.tensor(0.5, dtype=pred_ctr_y.dtype, device=pred_h.device) * pred_h
    pred_boxes = torch.stack((pred_boxes2, pred_boxes1, pred_boxes4, pred_boxes3), dim=2).flatten(1)

    return pred_boxes

class RCNNPostProcess(nn.Module):
    def __init__(self, conf_thres, nms_thres, coord_weights, num_class):
        super(RCNNPostProcess, self).__init__()
        self.conf_thres = conf_thres
        self.nms_thres = nms_thres
        self.num_detections = 100
        self.num_class = num_class
        self.bbox_xform_clip = math.log(1000/16)
        coord_weights = torch.tensor(coord_weights).view(1, 1, 4)
        self.register_buffer('coord_weights', coord_weights)

    def forward(self, box_reg, cls_scores, proposals, image_size, scale):
        cls_scores = cls_scores.detach()
        box_reg = box_reg.detach()

        cls_pred = F.softmax(cls_scores, dim=1)
        box_reg = box_reg.view(-1, self.num_class+1, 4) / self.coord_weights

        cls_labels = torch.arange(cls_pred.shape[1]).view(1, -1).expand_as(cls_pred)

        box_pred = decoding_box(box_reg.flatten(1), proposals, self.bbox_xform_clip)
        box_pred = box_pred.view(-1, self.num_class+1, 4)

        box_pred[..., 0::2] = torch.clamp(box_pred[..., 0::2], 0, image_size[0])
        box_pred[..., 1::2] = torch.clamp(box_pred[..., 1::2], 0, image_size[1])

        # eliminate BG
        scores = cls_pred[:, 1:].reshape(-1)
        labels = cls_labels[:, 1:].reshape(-1)
        boxes = box_pred[:, 1:, :] .reshape(-1, 4)

        # eliminate small confidence boxes
        conf_mask = scores > self.conf_thres

        scores = scores[conf_mask] # could be [0, :]
        labels = labels[conf_mask]
        boxes = boxes[conf_mask, :]

        if boxes.size(0) != 0:
            # eliminate small box
            size_mask = (boxes[:, 2]-boxes[:, 0] > 1e-2) & (boxes[:, 3]-boxes[:,1] > 1e-2)
            scores = scores[size_mask]
            labels = labels[size_mask]
            boxes = boxes[size_mask]

            # NMS
            nms_mask = torch.zeros_like(scores)
            for i in range(1, self.num_class+1):
                cls_ind = torch.nonzero(labels==i, as_tuple=True)[0]
                keep = torchvision.ops.nms(boxes[cls_ind], scores[cls_ind], self.nms_thres)
                nms_mask[cls_ind[keep]] = 1
            nms_ind = torch.nonzero(nms_mask, as_tuple=True)[0]

            scores = scores[nms_ind]
            labels = labels[nms_ind]
            boxes = boxes[nms_ind]

        scores, ind = torch.sort(scores, dim=0, descending=True)
        # scores = scores[:self.num_detections]
        labels = labels[ind]
        boxes = boxes[ind]

        # return boxes, labels, scores
        detections = {'boxes': boxes.cpu().numpy(),
            'labels': labels.cpu().numpy(),
            'scores': scores.cpu().numpy()}
        return detections

