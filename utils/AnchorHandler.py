import torch
import torch.nn as nn

class AnchorHandler(nn.Module):
    def __init__(self, basesize=16, scales=(8, 16, 32), ratios=(0.5, 1., 2.)):
        super(AnchorHandler, self).__init__()
        self.basesize = basesize
        self.scales = torch.tensor([s * basesize for s in scales]).float()
        self.ratios = torch.tensor(ratios).float()

    def build_base_anchors(self):
        h_ratios = torch.sqrt(self.ratios)
        w_ratios = 1 / h_ratios

        ws = (w_ratios[:, None] * self.scales[None, :]).view(-1)
        hs = (h_ratios[:, None] * self.scales[None, :]).view(-1)

        base_anchors = torch.stack([-hs, -ws, hs, ws], dim=1) / 2

        return base_anchors

    def tile_base_anchors(self, device, stride, cellsize):
        base_anchor = self.build_base_anchors().to(device)
        shifts_x = torch.arange(0, cellsize[1], device=device).float() * int(stride)
        shifts_y = torch.arange(0, cellsize[0], device=device).float() * int(stride)
        shift_y, shift_x = torch.meshgrid(shifts_y, shifts_x)
        shift_x = shift_x.reshape(-1)
        shift_y = shift_y.reshape(-1)
        shifts = torch.stack((shift_y, shift_x, shift_y, shift_x), dim=1)
        tiled_anchors = (shifts.view(-1, 1, 4) + base_anchor.view(1, -1, 4)).reshape(-1, 4)
        tiled_anchors += stride // 2
        return tiled_anchors

    def forward(self, device, stride, cellsize):
        return self.tile_base_anchors(device, stride, cellsize)

class PredBoxHandler(nn.Module):
    def __init__(self):
        super(PredBoxHandler).__init__()
