import torch
import sys, os
import numpy as np
from collections import OrderedDict
from tqdm import tqdm

from models import FasterRCNN_VGG16
from utils import AnchorHandler
from utils import RCNNPreProcess
from utils import RCNNPostProcess
from loss import FasterRCNNLoss
from MAPMeter import AveragePrecisionMeter

from dataloader import load_train_data
from dataloader import load_test_data
from eval_frcnn import init_model

# def init_model():
#     # initial a VGG16 frcnn
#     # by default, TOP4 conv layers are frozen
#     arch = FasterRCNN_VGG16()
#
#     return arch

def init_optimizer(model:torch.nn.Module, lr=1e-3, weight_decay=5e-4):
    params = []
    for k, v in dict(model.named_parameters()).items():
        if v.requires_grad:
            if 'bias' in k:
                params += [{'params': [v], 'lr': lr*2, 'weight_decay': 0}]
            else:
                params += [{'params': [v], 'lr': lr, 'weight_decay':weight_decay}]
    optimizer = torch.optim.SGD(params, momentum=0.9)

    return optimizer

def init_loss_fn():
    rpn_loss_cfg = {
        'conf_lb': 0.3,
        'conf_ub': 0.7,
        'num_prop': 256,
        'pos_ratio': 0.5
    }

    roi_loss_cfg = {
        'coord_weights': [10, 10, 5, 5],
        'conf_lb': 0.5,
        'conf_ub': 0.5,
        'num_prop': 128,
        'pos_ratio': 0.25
    }

    loss_fn = FasterRCNNLoss(rpn_loss_cfg, roi_loss_cfg)
    loss_fn = loss_fn.cuda()

    return loss_fn

def train_frcnn():
    train = load_train_data()
    val = load_test_data()

    anchor_handler = AnchorHandler()

    model = init_model()
    model = model.cuda()

    optimizer = init_optimizer(model=model)
    pre_process = RCNNPreProcess()

    loss_fn = init_loss_fn()
    mAP_meter = AveragePrecisionMeter(20)
    mAP_meter.initialize()

    evaluation_config = {
        'conf_thres':0.05,
        'nms_thres': 0.3,
        'coord_weights': [10, 10, 5, 5],
        'num_class': 20
    }
    post_process = RCNNPostProcess(**evaluation_config).cuda()

    STRIDE = 16

    num_epoch = 1
    for epoch in range(num_epoch):
        # train
        model.train()
        mAP_meter.clear()
        for i, batch in enumerate(tqdm(train)):
            batch = pre_process('cuda:0', batch)
            batch['feat_size'] = batch['images'].shape[-2:]
            # batch['images'].requires_grad = True
            batch['anchors'] = anchor_handler('cuda:0', STRIDE,
                (batch['feat_size'][0] // STRIDE, batch['feat_size'][1] // STRIDE))

            outs = model(batch)

            loss = loss_fn(outs, batch)
            print(loss)
            optimizer.zero_grad()
            loss['total_loss'].backward()
            optimizer.step()

            box_reg = outs['boxes']
            box_scores = outs['box_scores']
            proposals = outs['proposals']
            batch_index = outs['batch_index']

            for b in range(batch_index[-1]+1):
                mask = batch_index == b
                scale = batch['scales'][b]
                fea_size = batch['images'].shape[-2:]
                detection = post_process(box_reg[mask],
                    box_scores[mask], proposals[mask], fea_size, scale)
                detection['labels'] -= 1

                target = {'boxes': batch['boxes'][b].cpu().numpy(),
                    'labels': batch['labels'][b].cpu().numpy(),
                    'difficult': batch['difficult'][b], 'scale': scale}

                mAP_meter.accumulate(detection, target)
            break

if __name__ == '__main__':
    train_frcnn()
