import torch
import torch.nn as nn
import torch.nn.functional as F

class RPNHead(nn.Module):
    def __init__(self, in_channels, mid_channels, num_anchors):
        super(RPNHead, self).__init__()

        self.conv1 = nn.Conv2d(in_channels, mid_channels, 3, 1, 1)
        self.score = nn.Conv2d(mid_channels, num_anchors * 2, 1, 1, 0)
        self.loc = nn.Conv2d(mid_channels, num_anchors * 4, 1, 1, 0)

        for l in self.children():
            torch.nn.init.normal_(l.weight, 0, std=0.01)
            torch.nn.init.constant_(l.bias, 0)

    def forward(self, x):
        x = F.relu(self.conv1(x))

        rpn_obj = self.score(x)
        rpn_loc = self.loc(x)

        return rpn_obj, rpn_loc

import math
import torchvision

def decoding_box(reg, anchors, max_wh = math.log(1000 / 16.)):
    # use YXYX format, convert to CYCX
    heights = anchors[:, 2] - anchors[:, 0]
    widths = anchors[:, 3] - anchors[:, 1]
    ctr_y = anchors[:, 0] + 0.5 * heights
    ctr_x = anchors[:, 1] + 0.5 * widths

    dy = reg[:, 0::4]
    dx = reg[:, 1::4]
    dh = reg[:, 2::4]
    dw = reg[:, 3::4]

    # Prevent sending too large values into torch.exp()
    dw = torch.clamp(dw, max=max_wh)
    dh = torch.clamp(dh, max=max_wh)

    # Apply anchors
    pred_ctr_x = dx * widths[:, None] + ctr_x[:, None]
    pred_ctr_y = dy * heights[:, None] + ctr_y[:, None]
    pred_w = torch.exp(dw) * widths[:, None]
    pred_h = torch.exp(dh) * heights[:, None]
    # CYCX --> YXYX
    pred_boxes1 = pred_ctr_x - torch.tensor(0.5, dtype=pred_ctr_x.dtype, device=pred_w.device) * pred_w
    pred_boxes2 = pred_ctr_y - torch.tensor(0.5, dtype=pred_ctr_y.dtype, device=pred_h.device) * pred_h
    pred_boxes3 = pred_ctr_x + torch.tensor(0.5, dtype=pred_ctr_x.dtype, device=pred_w.device) * pred_w
    pred_boxes4 = pred_ctr_y + torch.tensor(0.5, dtype=pred_ctr_y.dtype, device=pred_h.device) * pred_h
    pred_boxes = torch.stack((pred_boxes2, pred_boxes1, pred_boxes4, pred_boxes3), dim=2).flatten(1)

    return pred_boxes

class ProposalCreator(nn.Module):
    def __init__(self,
                 nms_thresh=0.7,
                 n_train_pre_nms=12000,
                 n_train_post_nms=2000,
                 n_test_pre_nms=6000,
                 n_test_post_nms=300,
                 min_size=16):
        super(ProposalCreator, self).__init__()

        self.nms_thres = nms_thresh
        self.n_train_pre_nms = n_train_pre_nms
        self.n_train_post_nms = n_train_post_nms
        self.n_test_pre_nms = n_test_pre_nms
        self.n_test_post_nms = n_test_post_nms
        self.box_min_size = min_size * 1.5

        self.bbox_dwdh_clip = math.log(1000 / 16.)


    def _filter_anchors(self, anchors, H, W):
        index_inside = (anchor[:, 0] >= 0) & (anchor[:, 1] >= 0) & \
            (anchor[:, 2] <= H) & (anchor[:, 3] <= W)
        index_inside = index_inside.nonzero().squeeze(1)

        return anchors[index_inside].clone(), index_inside

    def forward(self, reg, scores, anchors, img_size):
        if self.training:
            n_pre_nms = self.n_train_pre_nms
            n_post_nms = self.n_train_post_nms
        else:
            n_pre_nms = self.n_test_pre_nms
            n_post_nms = self.n_test_post_nms

        # reg = reg[valid_index]
        boxes = decoding_box(reg, anchors, self.bbox_dwdh_clip)

        # NOTE: boxes are YXYX format
        pre_nms_boxes = torch.stack([
            boxes[..., 0::2].clamp(min=0, max=img_size[0]),
            boxes[..., 1::2].clamp(min=0, max=img_size[1])], dim=-1).view(-1, 4)

        wmask = (pre_nms_boxes[:, 3] - pre_nms_boxes[:, 1]) >= self.box_min_size
        hmask = (pre_nms_boxes[:, 2] - pre_nms_boxes[:, 0]) >= self.box_min_size
        valid_boxes_mask = (wmask & hmask).nonzero().squeeze(1)
        pre_nms_boxes = pre_nms_boxes[valid_boxes_mask]
        scores = scores[valid_boxes_mask]

        _, top_n_idx = scores.topk(min(n_pre_nms, scores.size(0)))
        pre_nms_boxes = pre_nms_boxes[top_n_idx]
        pre_nms_scores = scores[top_n_idx]

        keep = torchvision.ops.nms(pre_nms_boxes, pre_nms_scores, self.nms_thres)
        post_nms_boxes = pre_nms_boxes[keep, :]
        post_nms_scores = pre_nms_scores[keep]

        n_post_nms = min(n_post_nms, post_nms_boxes.size(0))

        return post_nms_boxes[:n_post_nms], post_nms_scores[:n_post_nms]


class RPN(nn.Module):
    def __init__(self, in_channels, mid_channels, num_anchors, proposer=None):
        super(RPN, self).__init__()

        self.head = RPNHead(in_channels, mid_channels, num_anchors)
        self.num_anchors = num_anchors

        if proposer is None:
            self.propose = ProposalCreator()
        else:
            self.propose = proposer

    def forward(self, x, anchors, sizes):
        n, _, hh, ww = x.shape
        n_anchor = anchors.size(0) // (hh * ww)

        rpn_scores, rpn_locs = self.head(x)

        rpn_locs = rpn_locs.permute(0, 2, 3, 1).contiguous().view(x.size(0), -1, 4)
        detached_locs = rpn_locs.detach()

        rpn_scores = rpn_scores.permute(0, 2, 3, 1).contiguous()
        rpn_scores = rpn_scores.view(n, hh, ww, n_anchor, 2)
        detached_scores = rpn_scores.detach()

        rpn_softmax_scores = F.softmax(detached_scores, dim=4)
        rpn_fg_scores = rpn_softmax_scores[:, :, :, :, 1].contiguous()
        rpn_fg_scores = rpn_fg_scores.view(n, -1)
        proposals = []
        batch_idx = []
        for i in range(x.size(0)):
            proposed_boxes, proposed_scores, = self.propose(detached_locs[i],
                    rpn_fg_scores[i], anchors, sizes)
            batch_idx.append(torch.ones(len(proposed_boxes), dtype=torch.int,
                device=proposed_boxes.device)*i)
            proposals.append(proposed_boxes)

        proposals = torch.cat(proposals, dim=0)
        indexes = torch.cat(batch_idx, dim=0)

        # raw rpn box reg, scores, proposals of all batch, index of proposals
        return rpn_locs, rpn_scores, proposals, indexes

