import torch
import torch.nn as nn
from torchvision.ops import RoIPool

class ROIHead(nn.Module):
    def __init__(self, in_size, spatial_scale):
        super(ROIHead, self).__init__()

        # NOTE: in_size -> size of feature map
        # NOTE: spatial_scale -> ratio of feature map to original image
        self.pool = RoIPool((in_size, in_size), spatial_scale)

    def roi_pool(self, x, proposals, batch_index):
        batch_proposals = torch.cat([batch_index[:, None].float(), proposals], dim=1)

        # NOTE: YXYX -> XYXY
        batch_proposal_xy = batch_proposals[:, [0, 2, 1, 4, 3]].contiguous()

        x = self.pool(x, batch_proposal_xy)
        x = x.flatten(1)

        return x


class VGG16ROIHead(ROIHead):
    def __init__(self, in_channel, num_class, classifier):
        super(VGG16ROIHead, self).__init__(7, 1/16)

        self.classifier = classifier
        self.cls_loc = nn.Linear(in_channel, (num_class+1)*4)
        self.score = nn.Linear(in_channel, num_class+1)

        for m in self.cls_loc.children():
            torch.nn.init.normal_(m.weight, 0, 0.001)
            torch.nn.init.constant_(m.bias, 0)
        for m in self.score.children():
            torch.nn.init.normal_(m.weight, 0, 0.01)
            torch.nn.init.constant_(m.bias, 0)

    def forward(self, x, proposals, batch_index):
        x = super(VGG16ROIHead, self).roi_pool(x, proposals, batch_index)

        fc7 = self.classifier(x)
        box_reg = self.cls_loc(fc7)
        box_score = self.score(fc7)

        return box_reg, box_score

