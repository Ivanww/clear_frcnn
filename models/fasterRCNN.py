import torch
import torch.nn as nn

from torchvision.models import vgg16

def init_VGG16_backbone():
    model = vgg16(pretrained=True)

    features = list(model.features)[:30]
    classifier = model.classifier

    classifier = list(classifier)
    del classifier[6]
    # delete dropout layers
    del classifier[5]
    del classifier[2]
    classifier = nn.Sequential(*classifier)

    # freeze top4 conv
    for layer in features[:10]:
        for p in layer.parameters():
            p.requires_grad = False

    return nn.Sequential(*features), classifier


class FasterRCNN(nn.Module):
    def __init__(self, backbone=None, rpn=None, head=None, proposal_sampler=None):
        super(FasterRCNN, self).__init__()

        self.backbone = backbone
        self.rpn = rpn
        self.head = head
        self.proposal_sampler = proposal_sampler

    def forward(self, x:dict):
        images = x['images']
        anchors = x['anchors']
        image_sizes = x['sizes']
        size = images.shape[-2:]  # scaled size

        outs = {}

        features = self.backbone(images)
        rpn_locs, rpn_scores, proposals, batch_index = self.rpn(features, anchors, size)
        outs.update({
            'rpn_scores': rpn_scores, 'rpn_reg': rpn_locs
        })

        # if training, sample the proposal for head, otherwise use all proposal for head
        if self.training:
            assert self.proposal_sampler is not None
            assert 'boxes' in x

            train_proposals = []
            proposal_boxes = []
            proposal_labels = []
            proposal_indexes = []
            for i in range(batch_index[-1]+1):
                mask = batch_index == i
                aug_proposals = torch.cat([proposals[mask], x['boxes'][i]], dim=0)
                sample_boxes, sample_labels = self.proposal_sampler(
                    x['boxes'][i], aug_proposals, true_labels=x['labels'][i]
                )

                train_mask = sample_labels != self.proposal_sampler.IGNORED

                train_proposals.append(aug_proposals[train_mask])
                proposal_boxes.append(sample_boxes[train_mask])
                proposal_labels.append(sample_labels[train_mask])
                proposal_indexes.append(torch.ones(train_mask.sum(),
                    device=proposals.device).long()*i)

            # update proposal
            proposals = torch.cat(train_proposals, dim=0)
            batch_index = torch.cat(proposal_indexes, dim=0) # batch index

            # keep proposal labels and proposal boxes as lists
            outs.update({'proposal_labels': proposal_labels,
                'proposal_boxes': proposal_boxes}) # this is only for training
        outs.update({'proposals': proposals, 'batch_index': batch_index})

        box_reg, box_scores = self.head(features, proposals, batch_index)
        outs.update({'boxes': box_reg, 'box_scores': box_scores})

        return outs

from .RPN import RPN
from .ROIHead import VGG16ROIHead
def FasterRCNN_VGG16(num_anchors=9, sampler=None):
    backbone, classifier = init_VGG16_backbone()
    rpn = RPN(512, 512, num_anchors)
    head = VGG16ROIHead(in_channel=4096, num_class=20, classifier=classifier)

    return FasterRCNN(backbone, rpn, head, sampler)
