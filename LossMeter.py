from MetricMeter import MetricMeter
class LossMeter(MetricMeter):
    def __init__(self):
        super(LossMeter, self).__init__()

    def initialize(self):
        self.cached_data = {'count': 0}

    def accumulate(self, loss, n=1):
        for k in loss:
            if k not in self.cached_data:
                self.cached_data[k] = loss[k].detach()
            else:
                self.cached_data[k] += loss[k].detach()
        self.cached_data['count'] += n


    def evaluate(self):
        metric = {}
        for k in self.cached_data:
            if k == 'count':
                continue
            metric[k] = round((self.cached_data[k] / self.cached_data['count']).cpu().item(), 4)

        return metric

    def clear(self):
        self.initialize()
